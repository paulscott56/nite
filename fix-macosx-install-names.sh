#!/bin/sh -e

NITE_VERSION_SUFFIX=1_5_2

run ()
{
    echo " $@"
    "$@"
}

if [ $# -lt 1 ]; then
	echo "Usage: $0 <install_prefix>"
	exit 1
fi

prefix="$1"

set_id () # dylib name
{
	dylib="$1"
	name="$2"

	run install_name_tool -id "$name" "$prefix/lib/$dylib"
}

mv_name () # dylib from to
{
	dylib="$1"
	from="$2"
	to="$3"

	run install_name_tool -change "$from" "$to" "$prefix/lib/$dylib"
}

set_id  libXnVCNITE_$NITE_VERSION_SUFFIX.dylib                                                @loader_path/../lib/libXnVCNITE_$NITE_VERSION_SUFFIX.dylib
mv_name libXnVCNITE_$NITE_VERSION_SUFFIX.dylib ../../Bin/x86-Release/libOpenNI.dylib          @loader_path/../lib/libOpenNI.dylib
mv_name libXnVCNITE_$NITE_VERSION_SUFFIX.dylib ../../Bin/x86-Release/libXnVNite_$NITE_VERSION_SUFFIX.dylib   @loader_path/../lib/libXnVNite_$NITE_VERSION_SUFFIX.dylib

set_id  libXnVFeatures_$NITE_VERSION_SUFFIX.dylib                                             @loader_path/../lib/libXnVFeatures_$NITE_VERSION_SUFFIX.dylib
mv_name libXnVFeatures_$NITE_VERSION_SUFFIX.dylib ../../Bin/x86-Release/libOpenNI.dylib       @loader_path/../lib/libOpenNI.dylib

set_id  libXnVHandGenerator_$NITE_VERSION_SUFFIX.dylib                                        @loader_path/../lib/libXnVHandGenerator_$NITE_VERSION_SUFFIX.dylib
mv_name libXnVHandGenerator_$NITE_VERSION_SUFFIX.dylib ../../Bin/x86-Release/libOpenNI.dylib  @loader_path/../lib/libOpenNI.dylib

set_id  libXnVNite_$NITE_VERSION_SUFFIX.dylib                                                 @loader_path/../lib/libXnVNite_$NITE_VERSION_SUFFIX.dylib
mv_name libXnVNite_$NITE_VERSION_SUFFIX.dylib ../../Bin/x86-Release/libOpenNI.dylib           @loader_path/../lib/libOpenNI.dylib
